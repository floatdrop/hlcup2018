FROM rust:1.31.0 as builder

# copy your source tree
COPY . ./

RUN RUSTFLAGS="-C target-cpu=nehalem" cargo build --release

FROM rust:1.31.0

COPY --from=builder \
    ./target/release/server \
    /usr/local/bin/

EXPOSE 80

STOPSIGNAL SIGTERM

ENV RUST_LOG "actix_web=error,hlcup2018=error,server=error"
ENV PORT 80

CMD ["/usr/local/bin/server"]

