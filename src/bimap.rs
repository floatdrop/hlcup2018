// https://gist.github.com/luki/335bf1195f5c003cd7872f62b67484da
use hashbrown::HashMap;
use std::hash::Hash;

pub struct BidirectionalMap<K, V> {
    pub right_to_left: HashMap<V, K>,
    pub left_to_right: HashMap<K, V>,
}

impl<K, V> BidirectionalMap<K, V>
where
    K: Hash + Eq + Clone,
    V: Hash + Eq + Clone,
{
    pub fn new() -> BidirectionalMap<K, V> {
        BidirectionalMap {
            right_to_left: HashMap::new(),
            left_to_right: HashMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.left_to_right.len()
    }

    pub fn get_value(&self, key: &K) -> Option<&V> {
        self.left_to_right.get(key)
    }

    pub fn get_key(&self, val: &V) -> Option<&K> {
        self.right_to_left.get(val)
    }

    pub fn insert(&mut self, k: K, v: V) -> Option<V> {
        self.right_to_left.insert(v.clone(), k.clone());
        self.left_to_right.insert(k, v)
    }
}
