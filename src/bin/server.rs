#[macro_use]
extern crate log;
extern crate actix;
extern crate actix_web;
extern crate env_logger;
extern crate hlcup;
extern crate serde;
extern crate serde_json;
extern crate zip;
#[macro_use]
extern crate serde_derive;
extern crate validator;
extern crate validator_derive;

use actix_web::{
    http::{header, HttpTryFrom, Method},
    middleware,
    middleware::{Middleware, Response, Started},
    server, HttpRequest, HttpResponse, Json, Path, Query, Result, State,
};
use failure::Error;
use std::sync::{Arc, RwLock};
use validator::Validate;

use hlcup::{json, Db};

#[derive(Deserialize)]
pub struct Info {
    id: u32,
}

pub struct App {
    current_time: u32,
    db: Arc<RwLock<Db>>, // TODO: Use RwLock (if needed)
}

fn suggest(
    info: Path<Info>,
    state: State<App>,
    params: Query<json::SuggestRequest>,
) -> HttpResponse {
    match params.validate() {
        Ok(_) => (),
        Err(_e) => return HttpResponse::BadRequest().into(),
    }

    let db = state.db.read().unwrap();

    if !db.exists(info.id) {
        return HttpResponse::NotFound().into();
    }

    match db.suggest(info.id, &params) {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn recommend(
    info: Path<Info>,
    state: State<App>,
    params: Query<json::RecommendRequest>,
) -> HttpResponse {
    match params.validate() {
        Ok(_) => (),
        Err(_e) => return HttpResponse::BadRequest().into(),
    }

    let db = state.db.read().unwrap();

    if !db.exists(info.id) {
        return HttpResponse::NotFound().into();
    }

    match db.recommend(info.id, &params, state.current_time) {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn filter(state: State<App>, params: Query<json::FilterRequest>) -> HttpResponse {
    match params.validate() {
        Ok(_) => (),
        Err(_e) => return HttpResponse::BadRequest().into(),
    }

    match state.db.read().unwrap().filter(&params, state.current_time) {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn group(state: State<App>, params: Query<json::GroupRequest>) -> HttpResponse {
    match params.validate() {
        Ok(_) => (),
        Err(_e) => return HttpResponse::BadRequest().into(),
    }

    match state.db.read().unwrap().group(&params) {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn new(state: State<App>, account: Json<json::Account>) -> HttpResponse {
    match state
        .db
        .write()
        .unwrap()
        .insert(&account, state.current_time)
    {
        Ok(_) => HttpResponse::Created().body("{}"),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn likes(state: State<App>, params: Json<json::LikesRequest>) -> HttpResponse {
    match state.db.write().unwrap().likes(&params.likes) {
        Ok(_) => HttpResponse::Accepted().body("{}"),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

fn update(state: State<App>, info: Path<Info>, params: Json<json::UpdateRequest>) -> HttpResponse {
    match params.validate() {
        Ok(_) => (),
        Err(_e) => return HttpResponse::BadRequest().into(),
    }

    {
        let db = state.db.read().unwrap();

        if !db.exists(info.id) {
            return HttpResponse::NotFound().into();
        }
    }

    match state
        .db
        .write()
        .unwrap()
        .update(info.id, &params, state.current_time)
    {
        Ok(()) => HttpResponse::Accepted().body("{}"),
        Err(_) => HttpResponse::BadRequest().into(),
    }
}

pub struct AppendServerHeader;

impl<S> Middleware<S> for AppendServerHeader {
    fn start(&self, _req: &HttpRequest<S>) -> Result<Started> {
        Ok(Started::Done)
    }

    fn response(&self, _req: &HttpRequest<S>, mut resp: HttpResponse) -> Result<Response> {
        resp.headers_mut().insert(
            header::HeaderName::try_from("Server").unwrap(),
            header::HeaderValue::from_static("actix-web"),
        );
        Ok(Response::Done(resp))
    }
}

fn main() {
    // ::std::env::set_var("RUST_LOG", "actix_web=warn,server=warn,hlcup=warn");
    env_logger::init();
    let sys = actix::System::new("hlcup");

    info!("Creating db...");

    let db = Arc::new(RwLock::new(Db::new()));

    let current_time = parse_data(&mut db.write().unwrap()).unwrap();

    let endpoint = format!(
        "0.0.0.0:{}",
        std::env::var("PORT").unwrap_or_else(|_| "8000".to_owned())
    );

    // Start http server
    server::new(move || {
        actix_web::App::with_state(App {
            current_time,
            db: db.clone(),
        })
        .middleware(middleware::Logger::default())
        .middleware(AppendServerHeader)
        .resource("/accounts/filter/", |r| r.method(Method::GET).with(filter))
        .resource("/accounts/group/", |r| r.method(Method::GET).with(group))
        .resource("/accounts/new/", |r| r.method(Method::POST).with(new))
        .resource("/accounts/likes/", |r| r.method(Method::POST).with(likes))
        .resource("/accounts/{id}/recommend/", |r| {
            r.method(Method::GET).with(recommend)
        })
        .resource("/accounts/{id}/suggest/", |r| {
            r.method(Method::GET).with(suggest)
        })
        .resource("/accounts/{id}/", |r| r.method(Method::POST).with(update))
    })
    .bind(&endpoint)
    .unwrap()
    .start();

    info!("Started server at http://{}", &endpoint);

    let _ = sys.run();
}

pub fn parse_data(db: &mut Db) -> Result<u32, Error> {
    use std::fs;
    use std::io::{BufReader, Read};
    use std::path::Path;
    use std::time::Instant;

    let now = Instant::now();

    let options = read(Path::new("/tmp/data/options.txt"))?;
    let current_time = options[0];
    info!("Current time: {}", current_time);

    let file = fs::File::open(Path::new("/tmp/data/data.zip"))?;
    let reader = BufReader::new(file);
    let mut archive = zip::ZipArchive::new(reader)?;

    info!("Parsing data from /tmp/data/data.zip");

    for i in (1..=archive.len()).rev() {
        let name = format!("accounts_{}.json", i);
        let mut part = archive.by_name(&name)?;

        debug!("Reading {} file", name);
        let mut contents = String::new();
        part.read_to_string(&mut contents)?;

        debug!("Parsing {} part", i);
        let p: json::Accounts = serde_json::from_str(&contents)?;
        debug!("Parsed {} accounts from {} part", p.accounts.len(), i);

        for account in &p.accounts {
            db.insert(account, current_time).unwrap();
        }
    }

    info!("Loaded in {}s", now.elapsed().as_secs());

    Ok(current_time)
}

use std::fs::File;
use std::io::{BufRead, BufReader, ErrorKind};

pub fn read(path: &std::path::Path) -> Result<Vec<u32>, Error> {
    let file = File::open(path)?;
    let br = BufReader::new(file);
    let mut v = Vec::new();

    for line in br.lines() {
        let line = line?;
        let n = line
            .trim()
            .parse()
            .map_err(|e| std::io::Error::new(ErrorKind::InvalidData, e))?;
        v.push(n);
    }
    Ok(v)
}
