extern crate chrono;
extern crate hashbrown;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate validator;
#[macro_use]
extern crate validator_derive;
extern crate bit_vec;
extern crate itertools;
extern crate vec_map;

use bit_vec::BitVec;
use chrono::prelude::*;
use hashbrown::{HashMap, HashSet};
use itertools::Itertools;
use std::cmp::Ordering;
use std::collections::BTreeMap;
use vec_map::VecMap;

mod bimap;
mod join_iterator;
pub mod json;
mod sorted_vec;
use self::bimap::BidirectionalMap;
use self::join_iterator::JoinIterator;
use self::json::*;
use self::sorted_vec::SortedVec;

pub struct Db {
    accounts: VecMap<AccountInfo>,
    cities: BidirectionalMap<String, u32>,
    countries: BidirectionalMap<String, u32>,
    interests: BidirectionalMap<String, u32>,
    fnames: BidirectionalMap<String, u32>,
    snames: BidirectionalMap<String, u32>,
    email_index: BTreeMap<String, u32>,
    sex_index: HashMap<Sex, SortedVec<u32>>,
    status_index: HashMap<Status, SortedVec<u32>>,
    country_index: HashMap<u32, SortedVec<u32>>,
    city_index: HashMap<u32, SortedVec<u32>>,
    interests_index: HashMap<u32, SortedVec<u32>>,
    liked_index: HashMap<u32, SortedVec<u32>>,
    premium_index: SortedVec<u32>,
    birth_year_index: HashMap<u32, SortedVec<u32>>,
    joined_year_index: HashMap<u32, SortedVec<u32>>,
    fname_index: HashMap<u32, SortedVec<u32>>,
    sname_index: HashMap<u32, SortedVec<u32>>,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct GroupKey {
    pub sex: Option<Sex>,
    pub status: Option<Status>,
    pub city: Option<u32>,
    pub country: Option<u32>,
    pub interests: Option<u32>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub enum RecommendStatus {
    FREE = 3,
    OCCUPIED = 1,
    COMPLICATED = 2,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct RecommendCandidate {
    pub premium_now: bool,
    pub status: RecommendStatus,
    pub common_interests: u8,
    pub age_diff: i32,
    pub id: u32,
}

fn get_key(index: &mut BidirectionalMap<String, u32>, key: String) -> u32 {
    let value = (index.len() + 1) as u32;
    match index.get_value(&key) {
        Some(value) => *value,
        None => {
            index.insert(key, value);
            value
        }
    }
}

impl Default for Db {
    fn default() -> Self {
        Self::new()
    }
}

impl Db {
    pub fn new() -> Self {
        Self {
            accounts: VecMap::new(),
            cities: BidirectionalMap::new(),
            countries: BidirectionalMap::new(),
            interests: BidirectionalMap::new(),
            fnames: BidirectionalMap::new(),
            snames: BidirectionalMap::new(),
            email_index: BTreeMap::new(),
            sex_index: vec![(Sex::F, SortedVec::new()), (Sex::M, SortedVec::new())]
                .into_iter()
                .collect(),
            status_index: vec![
                (Status::FREE, SortedVec::new()),
                (Status::OCCUPIED, SortedVec::new()),
                (Status::COMPLICATED, SortedVec::new()),
            ]
            .into_iter()
            .collect(),
            city_index: HashMap::new(),
            country_index: HashMap::new(),
            interests_index: HashMap::new(),
            liked_index: HashMap::new(),
            premium_index: SortedVec::new(),
            birth_year_index: HashMap::new(),
            joined_year_index: HashMap::new(),
            fname_index: HashMap::new(),
            sname_index: HashMap::new(),
        }
    }

    pub fn exists(&self, id: u32) -> bool {
        self.accounts.contains_key(id as usize)
    }

    pub fn filter(&self, params: &FilterRequest, time: u32) -> Result<FilterResults, ()> {
        let mut indexes: Vec<Box<dyn Iterator<Item = &u32>>> = vec![];

        if let Some(sex) = &params.sex_eq {
            indexes.push(Box::new(self.sex_index[sex].vec.iter()));
        };

        if let Some(status) = &params.status_eq {
            indexes.push(Box::new(self.status_index[status].vec.iter()));
        };

        if let Some(status) = &params.status_neq {
            let (ne1, ne2) = &match status {
                Status::FREE => (Status::OCCUPIED, Status::COMPLICATED),
                Status::COMPLICATED => (Status::FREE, Status::OCCUPIED),
                Status::OCCUPIED => (Status::FREE, Status::COMPLICATED),
            };
            indexes.push(Box::new(
                self.status_index[ne1]
                    .vec
                    .iter()
                    .merge_by(self.status_index[ne2].vec.iter(), |a, b| b <= a),
            ));
        };

        if let Some(country) = &params.country_eq {
            match self.countries.get_value(&country) {
                Some(c) => indexes.push(Box::new(self.country_index[c].vec.iter())),
                None => return Ok(FilterResults { accounts: vec![] }),
            }
        };

        match params.country_null {
            Some(IsNull::YES) => indexes.push(Box::new(self.country_index[&0].vec.iter())),
            // TODO: Investigate performance
            // Some(IsNull::NO) => indexes.push(Box::new(itertools::kmerge_by(
            //     self.country_index
            //         .iter()
            //         .filter(|(k, _)| **k > 0)
            //         .map(|(_, v)| v.iter().rev()),
            //     |a, b| b <= a,
            // ))),
            _ => (),
        };

        if let Some(city) = &params.city_eq {
            match self.cities.get_value(&city) {
                Some(c) => indexes.push(Box::new(self.city_index[c].vec.iter())),
                None => return Ok(FilterResults { accounts: vec![] }),
            }
        };

        match params.city_null {
            Some(IsNull::YES) => indexes.push(Box::new(self.city_index[&0].vec.iter())),
            // TODO: Investigate performance
            // Some(IsNull::NO) => indexes.push(Box::new(itertools::kmerge_by(
            //     self.city_index
            //         .iter()
            //         .filter(|(k, _)| **k > 0)
            //         .map(|(_, v)| v.iter().rev()),
            //     |a, b| b <= a,
            // ))),
            _ => (),
        };

        // TODO: if unknown city in city_any - should we bailout?
        if let Some(list) = &params.city_any {
            indexes.push(Box::new(itertools::kmerge_by(
                list.split(',')
                    .filter_map(|c| self.cities.get_value(&c.to_string()))
                    .map(|c| self.city_index[c].vec.iter()),
                |a, b| b <= a,
            )));
        };

        // TODO: if unknown interest in interests_any - should we bailout?
        if let Some(list) = &params.interests_any {
            indexes.push(Box::new(
                itertools::kmerge_by(
                    list.split(',')
                        .filter_map(|c| self.interests.get_value(&c.to_string()))
                        .map(|c| self.interests_index[c].vec.iter()),
                    |a, b| b <= a,
                )
                // TODO: Replace with linear unique
                .unique(),
            ));
        }

        if let Some(list) = &params.interests_contains {
            for i in list
                .split(',')
                .filter_map(|c| self.interests.get_value(&c.to_string()))
            {
                indexes.push(Box::new(self.interests_index[i].vec.iter()));
            }
        }

        if let Some(list) = &params.likes_contains {
            for i in list.split(',').map(|l| l.parse::<u32>().ok()) {
                match i {
                    Some(id) => indexes.push(match self.liked_index.get(&id) {
                        Some(index) => Box::new(index.vec.iter()),
                        None => return Ok(FilterResults { accounts: vec![] }),
                    }),
                    None => return Ok(FilterResults { accounts: vec![] }),
                }
            }
        }

        if params.premium_now.is_some() {
            indexes.push(Box::new(self.premium_index.vec.iter()));
        }

        if let Some(year) = params.birth_year {
            match self.birth_year_index.get(&(year as u32)) {
                Some(index) => indexes.push(Box::new(index.vec.iter())),
                None => return Ok(FilterResults { accounts: vec![] }),
            }
        };

        if let Some(name) = &params.fname_eq {
            match self.fnames.get_value(&name) {
                Some(c) => indexes.push(Box::new(self.fname_index[c].vec.iter())),
                None => return Ok(FilterResults { accounts: vec![] }),
            }
        };

        match params.fname_null {
            Some(IsNull::YES) => indexes.push(Box::new(self.fname_index[&0].vec.iter())),
            // TODO: Investigate performance
            // Some(IsNull::NO) => indexes.push(Box::new(itertools::kmerge_by(
            //     self.fname_index
            //         .iter()
            //         .filter(|(k, _)| **k > 0)
            //         .map(|(_, v)| v.iter().rev()),
            //     |a, b| b <= a,
            // ))),
            _ => (),
        };

        // TODO: if unknown interest in interests_any - should we bailout?
        if let Some(list) = &params.fname_any {
            indexes.push(Box::new(itertools::kmerge_by(
                list.split(',')
                    .filter_map(|c| self.fnames.get_value(&c.to_string()))
                    .map(|c| self.fname_index[c].vec.iter()),
                |a, b| b <= a,
            )));
        }

        if let Some(name) = &params.sname_eq {
            match self.fnames.get_value(&name) {
                Some(c) => indexes.push(Box::new(self.sname_index[c].vec.iter())),
                None => return Ok(FilterResults { accounts: vec![] }),
            }
        };

        match params.sname_null {
            Some(IsNull::YES) => indexes.push(Box::new(self.sname_index[&0].vec.iter())),
            // TODO: Investigate performance
            // Some(IsNull::NO) => indexes.push(Box::new(itertools::kmerge_by(
            //     self.sname_index
            //         .iter()
            //         .filter(|(k, _)| **k > 0)
            //         .map(|(_, v)| v.iter().rev()),
            //     |a, b| b <= a,
            // ))),
            _ => (),
        };

        if let Some(prefix) = &params.sname_starts {
            indexes.push(Box::new(itertools::kmerge_by(
                self.snames
                    .left_to_right
                    .keys()
                    .filter(|k| k.starts_with(prefix))
                    .filter_map(|c| self.snames.get_value(&c.to_string()))
                    .map(|c| self.sname_index[c].vec.iter()),
                |a, b| b <= a,
            )));
        }

        let mut filters: Box<dyn Iterator<Item = &AccountInfo>> = if indexes.is_empty() {
            Box::new(self.accounts.values().rev())
        } else {
            Box::new(JoinIterator::new(indexes).map(|id| &self.accounts[*id as usize]))
        };

        filters = match &params.sex_eq {
            Some(sex) => Box::new(filters.filter(move |account| *sex == account.sex)),
            None => filters,
        };

        filters = match &params.email_domain {
            Some(domain) => Box::new(
                filters.filter(move |account| domain == parse_email_domain(&account.email)),
            ),
            None => filters,
        };

        filters = match &params.email_lt {
            Some(email) => Box::new(filters.filter(move |account| account.email < *email)),
            None => filters,
        };

        filters = match &params.email_gt {
            Some(email) => Box::new(filters.filter(move |account| account.email > *email)),
            None => filters,
        };

        filters = match &params.status_eq {
            Some(status) => Box::new(filters.filter(move |account| *status == account.status)),
            None => filters,
        };

        filters = match &params.status_neq {
            Some(status) => Box::new(filters.filter(move |account| *status != account.status)),
            None => filters,
        };

        filters = match &params.fname_eq {
            Some(fname) => match self.fnames.get_value(fname) {
                Some(key) => Box::new(filters.filter(move |account| account.fname == *key)),
                None => return Ok(FilterResults { accounts: vec![] }),
            },
            None => filters,
        };

        filters = match &params.fname_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.fname == 0)),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.fname != 0)),
            None => filters,
        };

        filters = match &params.fname_any {
            Some(fnames) => {
                let hash: HashSet<&u32> = fnames
                    .split(',')
                    .filter_map(|name| self.fnames.get_value(&name.to_string()))
                    .collect();
                Box::new(filters.filter(move |account| hash.contains(&account.fname)))
            }
            None => filters,
        };

        filters = match &params.sname_eq {
            Some(sname) => match self.snames.get_value(sname) {
                Some(key) => Box::new(filters.filter(move |account| account.sname == *key)),
                None => return Ok(FilterResults { accounts: vec![] }),
            },
            None => filters,
        };

        filters = match &params.sname_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.sname == 0)),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.sname != 0)),
            None => filters,
        };

        filters =
            match &params.sname_starts {
                Some(prefix) => Box::new(filters.filter(move |account| {
                    match &self.snames.get_key(&account.sname) {
                        Some(name) => name.starts_with(prefix),
                        None => false,
                    }
                })),
                None => filters,
            };

        filters = match &params.phone_code {
            Some(code) => Box::new(filters.filter(move |account| match &account.phone {
                Some(phone) => code == parse_phone_code(phone),
                None => false,
            })),
            None => filters,
        };

        filters = match &params.phone_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.phone.is_none())),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.phone.is_some())),
            None => filters,
        };

        filters = match &params.birth_year {
            Some(year) => Box::new(filters.filter(move |account| account.birth_year() == *year)),
            None => filters,
        };

        filters = match &params.birth_lt {
            Some(birth) => Box::new(filters.filter(move |account| account.birth < *birth)),
            None => filters,
        };

        filters = match &params.birth_gt {
            Some(birth) => Box::new(filters.filter(move |account| account.birth > *birth)),
            None => filters,
        };

        filters = match &params.premium_now {
            Some(_) => Box::new(filters.filter(move |account| match account.premium {
                Some(premium) => premium.start < time && time < premium.finish,
                None => false,
            })),
            None => filters,
        };

        filters = match &params.premium_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.premium.is_none())),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.premium.is_some())),
            None => filters,
        };

        filters = match &params.country_eq {
            Some(country) => Box::new(filters.filter(move |account| {
                match self.countries.get_key(&account.country) {
                    Some(c) => country == c,
                    None => false,
                }
            })),
            None => filters,
        };

        filters = match &params.country_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.country == 0)),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.country != 0)),
            None => filters,
        };

        filters = match &params.city_eq {
            Some(city) => {
                Box::new(
                    filters.filter(move |account| match self.cities.get_key(&account.city) {
                        Some(c) => city == c,
                        None => false,
                    }),
                )
            }
            None => filters,
        };

        filters = match &params.city_null {
            Some(IsNull::YES) => Box::new(filters.filter(move |account| account.city == 0)),
            Some(IsNull::NO) => Box::new(filters.filter(move |account| account.city != 0)),
            None => filters,
        };

        filters = match &params.city_any {
            Some(cities) => {
                Box::new(
                    filters.filter(move |account| match self.cities.get_key(&account.city) {
                        Some(city) => cities.split(',').any(|c| c == city),
                        None => false,
                    }),
                )
            }
            None => filters,
        };

        filters = match &params.interests_contains {
            Some(list) => Box::new(filters.filter(move |account| {
                list.split(',')
                    .filter_map(|i| self.interests.get_value(&i.to_string()))
                    .all(|i| account.interests[*i as usize])
            })),
            None => filters,
        };

        filters = match &params.interests_any {
            Some(list) => {
                let ids = list
                    .split(',')
                    .filter_map(|i| self.interests.get_value(&i.to_string()))
                    .collect::<Vec<_>>();
                Box::new(
                    filters
                        .filter(move |account| ids.iter().any(|i| account.interests[**i as usize])),
                )
            }
            None => filters,
        };

        filters = match &params.likes_contains {
            Some(list) => Box::new(filters.filter(move |account| {
                list.split(',')
                    .filter_map(|i| i.parse().ok())
                    .all(|i: u32| account.likes.contains_key(&i))
            })),
            None => filters,
        };

        Ok(FilterResults {
            accounts: filters
                .map(|account| json::FilterResult {
                    id: account.id,
                    email: account.email.clone(),
                    fname: if params.has_fname() {
                        self.fnames.get_key(&account.fname).cloned()
                    } else {
                        None
                    },
                    sname: if params.has_sname() {
                        self.snames.get_key(&account.sname).cloned()
                    } else {
                        None
                    },
                    phone: if params.has_phone() {
                        account.phone.clone()
                    } else {
                        None
                    },
                    sex: if params.has_sex() {
                        Some(account.sex)
                    } else {
                        None
                    },
                    birth: if params.has_birth() {
                        Some(account.birth)
                    } else {
                        None
                    },
                    country: if params.has_country() {
                        self.countries.get_key(&account.country).cloned()
                    } else {
                        None
                    },
                    city: if params.has_city() {
                        self.cities.get_key(&account.city).cloned()
                    } else {
                        None
                    },
                    status: if params.has_status() {
                        Some(account.status)
                    } else {
                        None
                    },
                    premium: if params.has_premium() {
                        account.premium
                    } else {
                        None
                    },
                })
                .take(params.limit as usize)
                .collect::<Vec<_>>(),
        })
    }

    pub fn group(&self, params: &GroupRequest) -> Result<GroupResults, ()> {
        let mut groups = HashMap::with_capacity(10000);

        let has_sex = params.keys.iter().any(|c| c == "sex");
        let has_status = params.keys.iter().any(|c| c == "status");
        let has_city = params.keys.iter().any(|c| c == "city");
        let has_country = params.keys.iter().any(|c| c == "country");
        let has_interests = params.keys.iter().any(|c| c == "interests");

        // TODO: if keys length == 1 -> then we can do index only counting? Will filters interfere?

        let mut indexes: Vec<Box<dyn Iterator<Item = &u32>>> = vec![];

        if let Some(sex) = &params.sex {
            indexes.push(Box::new(self.sex_index[sex].vec.iter()));
        };

        if let Some(status) = &params.status {
            indexes.push(Box::new(self.status_index[status].vec.iter()));
        };

        if let Some(city) = &params.city {
            indexes.push(match self.cities.get_value(&city) {
                Some(c) => Box::new(self.city_index[c].vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        };

        if let Some(country) = &params.country {
            indexes.push(match self.countries.get_value(&country) {
                Some(c) => Box::new(self.country_index[c].vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        };

        if let Some(interest) = &params.interests {
            indexes.push(match self.interests.get_value(interest) {
                Some(i) => Box::new(self.interests_index[i].vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        }

        if let Some(year) = params.birth {
            indexes.push(match self.birth_year_index.get(&(year as u32)) {
                Some(index) => Box::new(index.vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        };

        if let Some(year) = params.joined {
            indexes.push(match self.joined_year_index.get(&(year as u32)) {
                Some(index) => Box::new(index.vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        };

        if let Some(likee) = params.likes {
            indexes.push(match self.liked_index.get(&likee) {
                Some(index) => Box::new(index.vec.iter()),
                None => return Ok(GroupResults { groups: vec![] }),
            });
        }

        if indexes.is_empty() && params.keys.len() == 1 {
            if has_sex {
                for (sex, index) in self.sex_index.iter() {
                    let group = GroupKey {
                        sex: Some(*sex),
                        status: None,
                        city: None,
                        country: None,
                        interests: None,
                    };

                    if index.len() > 0 {
                        let c = groups.entry(group).or_insert(0);
                        *c += index.len();
                    }
                }
            } else if has_status {
                for (status, index) in self.status_index.iter() {
                    let group = GroupKey {
                        status: Some(*status),
                        sex: None,
                        city: None,
                        country: None,
                        interests: None,
                    };

                    if index.len() > 0 {
                        let c = groups.entry(group).or_insert(0);
                        *c += index.len();
                    }
                }
            } else if has_city {
                for (city, index) in self.city_index.iter() {
                    let group = GroupKey {
                        city: Some(*city),
                        status: None,
                        sex: None,
                        country: None,
                        interests: None,
                    };

                    if index.len() > 0 {
                        let c = groups.entry(group).or_insert(0);
                        *c += index.len();
                    }
                }
            } else if has_country {
                for (country, index) in self.country_index.iter() {
                    let group = GroupKey {
                        country: Some(*country),
                        status: None,
                        city: None,
                        sex: None,
                        interests: None,
                    };

                    if index.len() > 0 {
                        let c = groups.entry(group).or_insert(0);
                        *c += index.len();
                    }
                }
            } else if has_interests {
                for (interest, index) in self.interests_index.iter() {
                    let group = GroupKey {
                        interests: Some(*interest),
                        status: None,
                        city: None,
                        country: None,
                        sex: None,
                    };

                    if index.len() > 0 {
                        let c = groups.entry(group).or_insert(0);
                        *c += index.len();
                    }
                }
            }
        } else {
            let mut filters: Box<dyn Iterator<Item = &AccountInfo>> = if indexes.is_empty() {
                Box::new(self.accounts.values().rev())
            } else {
                Box::new(JoinIterator::new(indexes).map(|id| &self.accounts[*id as usize]))
            };

            filters = match &params.sex {
                Some(sex) => Box::new(filters.filter(move |account| account.sex == *sex)),
                None => filters,
            };

            filters = match &params.status {
                Some(status) => Box::new(filters.filter(move |account| account.status == *status)),
                None => filters,
            };

            filters = match &params.city {
                Some(city) => match self.cities.get_value(city) {
                    Some(c) => Box::new(filters.filter(move |account| account.city == *c)),
                    None => Box::new(std::iter::empty()),
                },
                None => filters,
            };

            filters = match &params.country {
                Some(country) => match self.countries.get_value(country) {
                    Some(c) => Box::new(filters.filter(move |account| account.country == *c)),
                    None => Box::new(std::iter::empty()),
                },
                None => filters,
            };

            filters = match &params.interests {
                Some(interest) => match self.interests.get_value(interest) {
                    Some(key) => {
                        Box::new(filters.filter(move |account| account.interests[*key as usize]))
                    }
                    None => Box::new(std::iter::empty()),
                },
                None => filters,
            };

            filters = match &params.birth {
                Some(year) => {
                    Box::new(filters.filter(move |account| account.birth_year() == *year))
                }
                None => filters,
            };

            filters = match &params.joined {
                Some(year) => {
                    Box::new(filters.filter(move |account| account.joined_year() == *year))
                }
                None => filters,
            };

            filters = match &params.likes {
                Some(likee) => {
                    Box::new(filters.filter(move |account| account.likes.contains_key(likee)))
                }
                None => filters,
            };

            for account in filters {
                let mut key = GroupKey {
                    sex: if has_sex { Some(account.sex) } else { None },
                    status: if has_status {
                        Some(account.status)
                    } else {
                        None
                    },
                    city: if has_city { Some(account.city) } else { None },
                    country: if has_country {
                        Some(account.country)
                    } else {
                        None
                    },
                    interests: None,
                };

                if has_interests {
                    for (i, has) in account.interests.iter().enumerate() {
                        if has {
                            key.interests = Some(i as u32);
                            let c = groups.entry(key).or_insert(0);
                            *c += 1;
                        }
                    }
                } else {
                    let c = groups.entry(key).or_insert(0);
                    *c += 1;
                }
            }
        }

        let mut results = groups
            .iter()
            .map(|(key, count)| GroupResult {
                sex: key.sex,
                status: key.status,
                city: match &key.city {
                    Some(c) => self.cities.get_key(c).cloned(),
                    None => None,
                },
                country: match &key.country {
                    Some(c) => self.countries.get_key(c).cloned(),
                    None => None,
                },
                interests: match &key.interests {
                    Some(i) => self.interests.get_key(i).cloned(),
                    None => None,
                },
                count: *count,
            })
            .collect::<Vec<_>>();

        // TODO: Lazysort
        results.sort_by(|a, b| match params.order {
            Order::DESC => b.dynamic_cmp(&a, &params.keys),
            Order::ASC => a.dynamic_cmp(&b, &params.keys),
        });

        Ok(GroupResults {
            groups: results.into_iter().take(params.limit as usize).collect(),
        })
    }

    pub fn suggest(&self, id: u32, params: &SuggestRequest) -> Result<SuggestResults, ()> {
        let target = &self.accounts[id as usize];

        let mut indexes: Vec<Box<dyn Iterator<Item = &u32>>> = vec![];

        // indexes.push(Box::new(self.sex_index[&target.sex].iter().rev()));

        if let Some(city) = &params.city {
            indexes.push(match self.cities.get_value(&city) {
                Some(c) => Box::new(self.city_index[c].vec.iter()),
                None => return Ok(SuggestResults { accounts: vec![] }),
            });
        };

        if let Some(country) = &params.country {
            indexes.push(match self.countries.get_value(&country) {
                Some(c) => Box::new(self.country_index[c].vec.iter()),
                None => return Ok(SuggestResults { accounts: vec![] }),
            });
        };

        let my_likes = target.likes.iter().collect::<HashMap<_, _>>();

        indexes.push(Box::new(
            itertools::kmerge_by(
                target
                    .likes
                    .keys()
                    .map(|like| self.liked_index[like].vec.iter()),
                |a, b| b <= a,
            )
            // TODO: Replace by linear unique
            .unique(),
        ));

        let mut results: Box<dyn Iterator<Item = &AccountInfo>> = Box::new(
            JoinIterator::new(indexes)
                .map(|id| &self.accounts[*id as usize])
                .filter(|account| account.id != target.id && account.sex == target.sex),
        );

        results = match &params.city {
            Some(city) => match self.cities.get_value(city) {
                Some(c) => Box::new(results.filter(move |account| account.city == *c)),
                None => return Ok(SuggestResults { accounts: vec![] }),
            },
            None => results,
        };

        results = match &params.country {
            Some(country) => match self.countries.get_value(country) {
                Some(c) => Box::new(results.filter(move |account| account.country == *c)),
                None => return Ok(SuggestResults { accounts: vec![] }),
            },
            None => results,
        };

        let mut users = results
            .filter_map(|account| {
                let mut f = 0f64;

                for (like_id, like_ts) in &account.likes {
                    if let Some(my_ts) = my_likes.get(like_id) {
                        f += 1.0 / ((i64::from(**my_ts) - i64::from(*like_ts)).abs() as f64);
                    }
                }

                if f == 0.0 {
                    return None;
                }

                Some((account.id, f))
            })
            .collect::<Vec<_>>();

        // TODO: Lazysort
        users.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Ordering::Less));

        Ok(SuggestResults {
            accounts: users
                .iter()
                .map(|(id, _)| {
                    self.accounts[*id as usize]
                        .likes
                        .keys()
                        .rev()
                        .filter(|id| !target.likes.contains_key(*id))
                })
                .flatten()
                .take(params.limit as usize)
                .map(|id| {
                    let account = &self.accounts[*id as usize];
                    SuggestResult {
                        id: account.id,
                        email: account.email.clone(),
                        fname: self.fnames.get_key(&account.fname).cloned(),
                        sname: self.snames.get_key(&account.sname).cloned(),
                        status: account.status,
                    }
                })
                .collect(),
        })
    }

    pub fn recommend(
        &self,
        id: u32,
        params: &RecommendRequest,
        current_time: u32,
    ) -> Result<RecommendResults, ()> {
        let target = &self.accounts[id as usize];

        let mut indexes: Vec<Box<dyn Iterator<Item = &u32>>> = vec![];

        // indexes.push(Box::new(
        //     self.sex_index[match target.sex {
        //         Sex::F => &Sex::M,
        //         Sex::M => &Sex::F,
        //     }]
        //     .iter()
        //     .rev(),
        // ));

        if let Some(city) = &params.city {
            indexes.push(match self.cities.get_value(&city) {
                Some(c) => Box::new(self.city_index[c].vec.iter()),
                None => return Ok(RecommendResults { accounts: vec![] }),
            });
        };

        if let Some(country) = &params.country {
            indexes.push(match self.countries.get_value(&country) {
                Some(c) => Box::new(self.country_index[c].vec.iter()),
                None => return Ok(RecommendResults { accounts: vec![] }),
            });
        };

        indexes.push(Box::new(
            itertools::kmerge_by(
                target
                    .interests
                    .iter()
                    .enumerate()
                    .filter(|(_, has)| *has)
                    .map(|(i, _)| self.interests_index[&(i as u32)].vec.iter()),
                |a, b| b <= a,
            )
            // TODO: Replace by linear unique
            .unique(),
        ));

        let mut results: Box<dyn Iterator<Item = &AccountInfo>> = Box::new(
            JoinIterator::new(indexes)
                .map(|id| &self.accounts[*id as usize])
                .filter(|account| account.id != target.id && account.sex != target.sex),
        );

        results = match &params.city {
            Some(city) => match self.cities.get_value(city) {
                Some(c) => Box::new(results.filter(move |account| account.city == *c)),
                None => return Ok(RecommendResults { accounts: vec![] }),
            },
            None => results,
        };

        results = match &params.country {
            Some(country) => match self.countries.get_value(country) {
                Some(c) => Box::new(results.filter(move |account| account.country == *c)),
                None => return Ok(RecommendResults { accounts: vec![] }),
            },
            None => results,
        };

        let mut users = results
            .filter_map(|account| {
                let mut interests = account.interests.clone();
                interests.intersect(&target.interests);

                let common_interests = interests.iter().filter(|x| *x).count() as u8;
                if common_interests == 0 {
                    return None;
                }

                Some(RecommendCandidate {
                    id: account.id,
                    status: match account.status {
                        Status::FREE => RecommendStatus::FREE,
                        Status::OCCUPIED => RecommendStatus::OCCUPIED,
                        Status::COMPLICATED => RecommendStatus::COMPLICATED,
                    },
                    premium_now: match account.premium {
                        Some(premium) => {
                            premium.start < current_time && premium.finish > current_time
                        }
                        None => false,
                    },
                    common_interests,
                    age_diff: -(i64::from(account.birth) - i64::from(target.birth)).abs() as i32,
                })
            })
            .collect::<Vec<_>>();

        // TODO: Lazysort
        users.sort_by(|a, b| b.cmp(&a));

        Ok(RecommendResults {
            accounts: users
                .into_iter()
                .map(|t| {
                    let account = &self.accounts[t.id as usize];
                    RecommendResult {
                        id: account.id,
                        email: account.email.clone(),
                        status: account.status,
                        fname: self.fnames.get_key(&account.fname).cloned(),
                        sname: self.snames.get_key(&account.sname).cloned(),
                        birth: account.birth,
                        premium: account.premium,
                    }
                })
                .take(params.limit as usize)
                .collect(),
        })
    }

    pub fn likes(&mut self, likes: &[Like]) -> Result<(), ()> {
        for like in likes.iter() {
            if !self.exists(like.likee) || !self.exists(like.liker) {
                return Err(());
            }
        }

        for like in likes.into_iter() {
            self.liked_index
                .entry(like.likee)
                .or_insert_with(|| SortedVec::new())
                .insert(like.liker);

            if let Some(account) = self.accounts.get_mut(like.liker as usize) {
                account
                    .likes
                    .entry(like.likee)
                    .and_modify(|ts| {
                        *ts += like.ts;
                        *ts /= 2;
                    })
                    .or_insert(like.ts);
            }
        }

        Ok(())
    }

    pub fn update(&mut self, id: u32, params: &UpdateRequest, time: u32) -> Result<(), ()> {
        match &params.email {
            Some(email) => {
                if self.email_index.contains_key(email) {
                    return Err(());
                }
            }
            None => (),
        }

        match &params.likes {
            Some(likes) => {
                for like in likes.iter() {
                    if !self.exists(like.id) {
                        return Err(());
                    }
                }
            }
            None => (),
        }

        let account = match self.accounts.get_mut(id as usize) {
            Some(account) => account,
            None => return Err(()),
        };

        match &params.email {
            Some(email) => {
                self.email_index.remove(&account.email);
                account.email = email.to_string();
                self.email_index.insert(email.to_string(), id);
            }
            None => (),
        }

        match &params.likes {
            Some(likes) => {
                for like in account.likes.keys() {
                    self.liked_index
                        .entry(*like)
                        .or_insert_with(|| SortedVec::new())
                        .remove(&account.id);
                }

                account.likes.clear();

                for like in likes.into_iter() {
                    self.liked_index
                        .entry(like.id)
                        .or_insert_with(|| SortedVec::new())
                        .insert(account.id);

                    account
                        .likes
                        .entry(like.id)
                        .and_modify(|ts| {
                            *ts += like.ts;
                            *ts /= 2;
                        })
                        .or_insert(like.ts);
                }
            }
            None => (),
        }

        match &params.city {
            Some(city) => {
                self.city_index
                    .entry(account.city)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                let key = get_key(&mut self.cities, city.to_string());
                account.city = key;
                self.city_index
                    .entry(key)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.country {
            Some(country) => {
                self.country_index
                    .entry(account.country)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                let key = get_key(&mut self.countries, country.to_string());
                account.country = key;
                self.country_index
                    .entry(key)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.fname {
            Some(fname) => {
                self.fname_index
                    .entry(account.fname)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                let key = get_key(&mut self.fnames, fname.to_string());
                account.fname = key;
                self.fname_index
                    .entry(key)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.sname {
            Some(sname) => {
                self.sname_index
                    .entry(account.sname)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                let key = get_key(&mut self.snames, sname.to_string());
                account.sname = key;
                self.sname_index
                    .entry(key)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.phone {
            Some(phone) => account.phone = Some(phone.to_string()),
            None => (),
        }

        match &params.sex {
            Some(sex) => {
                self.sex_index
                    .entry(account.sex)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                account.sex = *sex;
                self.sex_index
                    .entry(*sex)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.status {
            Some(status) => {
                self.status_index
                    .entry(account.status)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                account.status = *status;
                self.status_index
                    .entry(*status)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.birth {
            Some(birth) => {
                self.birth_year_index
                    .entry(Utc.timestamp(account.birth as i64, 0).year() as u32)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                account.birth = *birth;
                self.birth_year_index
                    .entry(Utc.timestamp(account.birth as i64, 0).year() as u32)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.joined {
            Some(joined) => {
                self.joined_year_index
                    .entry(Utc.timestamp(account.joined as i64, 0).year() as u32)
                    .or_insert_with(|| SortedVec::new())
                    .remove(&account.id);
                account.joined = *joined;
                self.joined_year_index
                    .entry(Utc.timestamp(account.joined as i64, 0).year() as u32)
                    .or_insert_with(|| SortedVec::new())
                    .insert(account.id);
            }
            None => (),
        }

        match &params.premium {
            Some(premium) => {
                if premium.start < time && time < premium.finish {
                    self.premium_index.insert(account.id);
                } else {
                    self.premium_index.remove(&account.id);
                }
                account.premium = Some(*premium);
            }
            None => (),
        }

        match &params.interests {
            Some(interests) => {
                for (i, has) in account.interests.iter().enumerate() {
                    if has {
                        self.interests_index
                            .entry(i as u32)
                            .or_insert_with(|| SortedVec::new())
                            .remove(&account.id);
                    }
                }
                account.interests.clear();
                for interest in interests {
                    let key = get_key(&mut self.interests, interest.to_string());
                    self.interests_index
                        .entry(key)
                        .or_insert_with(|| SortedVec::new())
                        .insert(account.id);
                    account.interests.set(key as usize, true);
                }
            }
            None => (),
        }

        Ok(())
    }

    pub fn insert(&mut self, account: &Account, time: u32) -> Result<(), ()> {
        if self.exists(account.id) {
            return Err(());
        }

        if self.email_index.contains_key(&account.email) {
            return Err(());
        }

        let mut new_account = AccountInfo {
            id: account.id,
            sex: account.sex,
            status: account.status,
            city: match &account.city {
                Some(city) => get_key(&mut self.cities, city.to_string()),
                None => 0,
            },
            country: match &account.country {
                Some(country) => get_key(&mut self.countries, country.to_string()),
                None => 0,
            },
            birth: account.birth,
            joined: account.joined,
            email: account.email.clone(),
            sname: match &account.sname {
                Some(name) => get_key(&mut self.snames, name.to_string()),
                None => 0,
            },
            fname: match &account.fname {
                Some(name) => get_key(&mut self.fnames, name.to_string()),
                None => 0,
            },
            premium: account.premium,
            interests: BitVec::from_elem(128, false),
            phone: account.phone.clone(),
            likes: BTreeMap::new(),
        };

        for interest in &account.interests {
            let key = get_key(&mut self.interests, interest.to_string());
            self.interests_index
                .entry(key)
                .or_insert_with(|| SortedVec::new())
                .insert(new_account.id);
            new_account.interests.set(key as usize, true);
        }

        for like in &account.likes {
            self.liked_index
                .entry(like.id)
                .or_insert_with(|| SortedVec::new())
                .insert(account.id);

            new_account
                .likes
                .entry(like.id)
                .and_modify(|ts| {
                    *ts += like.ts;
                    *ts /= 2;
                })
                .or_insert(like.ts);
        }

        // new_account.likes.shrink_to_fit();

        self.email_index.insert(account.email.clone(), account.id);
        self.sex_index
            .entry(account.sex)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);
        self.status_index
            .entry(account.status)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);
        self.city_index
            .entry(new_account.city)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);
        self.country_index
            .entry(new_account.country)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);
        self.fname_index
            .entry(new_account.fname)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);
        self.sname_index
            .entry(new_account.sname)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);

        let birth_year = Utc.timestamp(account.birth as i64, 0).year() as u32;
        self.birth_year_index
            .entry(birth_year)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);

        let joined_year = Utc.timestamp(account.joined as i64, 0).year() as u32;
        self.joined_year_index
            .entry(joined_year)
            .or_insert_with(|| SortedVec::new())
            .insert(account.id);

        if let Some(premium) = account.premium {
            if premium.start < time && time < premium.finish {
                self.premium_index.insert(account.id);
            }
        }

        self.accounts.insert(account.id as usize, new_account);

        Ok(())
    }
}

pub struct AccountInfo {
    pub id: u32,
    pub sex: Sex,
    pub status: Status,
    pub city: u32,
    pub country: u32,
    pub birth: u32,
    pub joined: u32,
    pub email: String,
    pub phone: Option<String>,
    pub fname: u32,
    pub sname: u32,
    pub premium: Option<PremiumSpan>,
    pub likes: BTreeMap<u32, u32>,
    // TODO: This is useless with interests_index
    pub interests: BitVec,
}

impl AccountInfo {
    pub fn birth_year(&self) -> i32 {
        Utc.timestamp(i64::from(self.birth), 0).year()
    }

    pub fn joined_year(&self) -> i32 {
        Utc.timestamp(i64::from(self.joined), 0).year()
    }
}

fn parse_email_domain(email: &str) -> &str {
    match email.find('@') {
        Some(start) => &email[start + 1..],
        None => email,
    }
}

fn parse_phone_code(phone: &str) -> &str {
    match phone.find('(') {
        Some(start) => match phone[start + 1..].find(')') {
            Some(end) => &phone[start + 1..=start + end],
            None => "",
        },
        None => "",
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_email_domain() {
        assert_eq!(parse_email_domain("a@b.com"), "b.com");
        assert_eq!(parse_email_domain("a@"), "");
        assert_eq!(parse_email_domain("a"), "a");
    }

    #[test]
    fn test_parse_phone_code() {
        assert_eq!(parse_phone_code("8(999)11111111"), "999");
        assert_eq!(parse_phone_code("8(abc)11111111"), "abc");
        assert_eq!(parse_phone_code("()"), "");
        assert_eq!(parse_phone_code("floatdrop"), "");
    }
}
