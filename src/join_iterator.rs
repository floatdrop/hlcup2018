pub struct JoinIterator<'a> {
    iterators: Vec<Box<dyn Iterator<Item = &'a u32> + 'a>>,
    ids: Vec<Option<&'a u32>>,
}

impl<'a> JoinIterator<'a> {
    pub fn new(iterators: Vec<Box<dyn Iterator<Item = &'a u32> + 'a>>) -> Self {
        let mut obj = Self {
            iterators,
            ids: Vec::new(),
        };

        obj.iterators
            .sort_by(|a, b| a.size_hint().1.cmp(&b.size_hint().1));
        if let Some(ub) = obj.iterators[0].size_hint().1 {
            obj.iterators.retain(|index| {
                index.size_hint().1.is_some() && index.size_hint().1.unwrap() < ub * 2
            });
        }

        obj.ids = obj.iterators.iter_mut().map(|iter| iter.next()).collect();
        obj
    }
}

impl<'a> Iterator for JoinIterator<'a> {
    type Item = &'a u32;

    fn next(&mut self) -> Option<&'a u32> {
        if self.ids.is_empty() {
            return None;
        }

        let mut min = self.ids[0];

        // TODO: Use heap
        for id in self.ids.iter().skip(1) {
            if id < &min {
                min = *id;
            }
        }

        if min == None {
            return None;
        }

        // Search for id, that will be in all iterators
        while !self.ids.iter().all(|id| id == &min) {
            for (i, iter) in self.iterators.iter_mut().enumerate() {
                while self.ids[i] > min {
                    self.ids[i] = iter.next();
                }

                if self.ids[i] < min {
                    min = self.ids[i];
                }

                if self.ids[i] == None {
                    return None;
                }
            }
        }

        // Advance all iterators
        for (i, iter) in self.iterators.iter_mut().enumerate() {
            self.ids[i] = iter.next();
        }

        min
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple_sequence() {
        let a: Vec<u32> = vec![4, 2, 1];
        let b: Vec<u32> = vec![3, 2, 0];
        let mut join = JoinIterator::new(vec![Box::new(a.iter()), Box::new(b.iter())]);
        assert_eq!(join.next(), Some(&2u32));
        assert_eq!(join.next(), None);
        assert_eq!(join.next(), None);
    }

    #[test]
    fn test_empty() {
        let a: Vec<u32> = vec![];
        let b: Vec<u32> = vec![3, 2, 0];
        let mut join = JoinIterator::new(vec![Box::new(a.iter()), Box::new(b.iter())]);
        assert_eq!(join.next(), None);
    }

    #[test]
    fn test_short() {
        let a: Vec<u32> = vec![0];
        let b: Vec<u32> = vec![3, 2, 0];
        let mut join = JoinIterator::new(vec![Box::new(a.iter()), Box::new(b.iter())]);
        assert_eq!(join.next(), Some(&0u32));
        assert_eq!(join.next(), None);
    }

    #[test]
    fn test_triplet() {
        let a: Vec<u32> = vec![4, 3, 0];
        let b: Vec<u32> = vec![4, 2, 1, 0];
        let c: Vec<u32> = vec![4, 1, 0];
        let mut join = JoinIterator::new(vec![
            Box::new(a.iter()),
            Box::new(b.iter()),
            Box::new(c.iter()),
        ]);
        assert_eq!(join.next(), Some(&4u32));
        assert_eq!(join.next(), Some(&0u32));
        assert_eq!(join.next(), None);
    }
}
