pub struct SortedVec<T> {
    pub vec: Vec<T>,
}

impl<T> SortedVec<T>
where
    T: Ord + Eq + Clone,
{
    pub fn new() -> SortedVec<T> {
        SortedVec { vec: vec![] }
    }

    pub fn insert(&mut self, v: T) {
        match self.vec.binary_search_by(|probe| v.cmp(probe)) {
            Ok(_) => {} // element already in vector @ `pos`
            Err(pos) => self.vec.insert(pos, v),
        }
    }

    pub fn remove(&mut self, v: &T) {
        self.vec.retain(|i| i != v);
    }

    pub fn len(&self) -> usize {
        self.vec.len()
    }
}
