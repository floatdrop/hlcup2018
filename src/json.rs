use validator::{Validate, ValidationError};

#[derive(Deserialize)]
pub struct Accounts {
    pub accounts: Vec<Account>,
}

#[derive(Deserialize)]
pub struct Account {
    pub id: u32,
    pub email: String,
    pub fname: Option<String>,
    pub sname: Option<String>,
    pub phone: Option<String>,
    pub sex: Sex,
    pub birth: u32,
    pub country: Option<String>,
    pub city: Option<String>,
    pub joined: u32,
    pub status: Status,
    pub premium: Option<PremiumSpan>,

    #[serde(default)]
    pub likes: Vec<ShortLike>,

    #[serde(default)]
    pub interests: Vec<String>,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub struct PremiumSpan {
    pub start: u32,
    pub finish: u32,
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Sex {
    F = 0,
    M = 1,
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Debug)]
pub enum Status {
    #[serde(rename = "свободны")]
    FREE = 3,
    #[serde(rename = "заняты")]
    OCCUPIED = 2,
    #[serde(rename = "всё сложно")]
    COMPLICATED = 1,
}

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct ShortLike {
    pub id: u32,
    pub ts: u32,
}

#[derive(Serialize, Deserialize)]
pub struct Like {
    pub liker: u32,
    pub likee: u32,
    pub ts: u32,
}

#[derive(Deserialize)]
pub struct LikesRequest {
    pub likes: Vec<Like>,
}

#[derive(Deserialize, Validate)]
#[serde(deny_unknown_fields)]
pub struct UpdateRequest {
    #[validate(contains = "@")]
    pub email: Option<String>,
    pub fname: Option<String>,
    pub sname: Option<String>,
    pub phone: Option<String>,
    pub sex: Option<Sex>,
    pub birth: Option<u32>,
    pub country: Option<String>,
    pub city: Option<String>,
    pub joined: Option<u32>,
    pub status: Option<Status>,
    pub interests: Option<Vec<String>>,
    pub premium: Option<PremiumSpan>,
    pub likes: Option<Vec<ShortLike>>,
}

impl UpdateRequest {
    pub fn fields_modified(&self) -> bool {
        self.email.is_some()
            || self.fname.is_some()
            || self.sname.is_some()
            || self.phone.is_some()
            || self.sex.is_some()
            || self.birth.is_some()
            || self.country.is_some()
            || self.city.is_some()
            || self.joined.is_some()
            || self.status.is_some()
            || self.premium.is_some()
    }
}

#[derive(Deserialize, Validate)]
#[serde(deny_unknown_fields)]
pub struct SuggestRequest {
    // Служебное поле
    pub query_id: u32,

    // Фильтры по конкретным значениям
    #[validate(length(min = "1"))]
    pub country: Option<String>,
    #[validate(length(min = "1"))]
    pub city: Option<String>,

    // Дополнительные параметры запроса
    #[validate(range(min = "1", max = "20"))]
    pub limit: u8,
}

#[derive(Serialize)]
pub struct SuggestResults {
    pub accounts: Vec<SuggestResult>,
}

#[derive(Serialize, Clone)]
pub struct SuggestResult {
    pub id: u32,
    pub email: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fname: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sname: Option<String>,
    pub status: Status,
}

#[derive(Deserialize, Validate)]
#[serde(deny_unknown_fields)]
pub struct GroupRequest {
    // Служебное поле
    pub query_id: u32,

    // Колонки для группировки
    #[validate(custom = "validate_group_keys")]
    #[serde(deserialize_with = "comma_separated")]
    pub keys: Vec<String>,

    // Фильтры по конкретным значениям
    pub country: Option<String>,
    pub city: Option<String>,
    pub interests: Option<String>,
    pub sex: Option<Sex>,
    pub status: Option<Status>,
    pub likes: Option<u32>,
    pub birth: Option<i32>,
    pub joined: Option<i32>,

    // Дополнительные параметры запроса
    #[validate(range(min = "1", max = "50"))]
    pub limit: u8,
    pub order: Order,
}

#[derive(Serialize)]
pub struct GroupResults {
    pub groups: Vec<GroupResult>,
}

#[derive(Serialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct GroupResult {
    pub count: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sex: Option<Sex>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<Status>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interests: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub country: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub city: Option<String>,
}

use std::cmp::Ordering;

impl GroupResult {
    pub fn dynamic_cmp<T: AsRef<str>>(&self, b: &GroupResult, keys: &[T]) -> Ordering {
        let count_cmp = self.count.cmp(&b.count);
        if count_cmp != Ordering::Equal {
            return count_cmp;
        }

        for key in keys {
            let cmp = match key.as_ref() {
                "sex" => self.sex.cmp(&b.sex),
                "status" => self.status.cmp(&b.status),
                "city" => self.city.cmp(&b.city),
                "country" => self.country.cmp(&b.country),
                "interests" => self.interests.cmp(&b.interests),
                _ => Ordering::Equal,
            };

            if cmp != Ordering::Equal {
                return cmp;
            }
        }

        Ordering::Equal
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq)]
pub enum Order {
    #[serde(rename = "-1")]
    DESC = -1,
    #[serde(rename = "1")]
    ASC = 1,
}

#[derive(Deserialize, Validate, Debug)]
#[serde(deny_unknown_fields)]
pub struct FilterRequest {
    // Служебное поле
    pub query_id: u32,

    #[validate(range(min = "1", max = "50"))]
    pub limit: u8,

    // соответствие конкретному полу - "m" или "f";
    pub sex_eq: Option<Sex>,

    // выбрать всех, чьи email-ы имеют указанный домен;
    pub email_domain: Option<String>,
    // выбрать всех, чьи email-ы лексикографически раньше;
    pub email_lt: Option<String>,
    // то же, но лексикографически позже;
    pub email_gt: Option<String>,

    // соответствие конкретному статусу;
    pub status_eq: Option<Status>,
    // выбрать всех, чей статус не равен указанному;
    pub status_neq: Option<Status>,

    // соответствие конкретному имени;
    pub fname_eq: Option<String>,
    // соответствие любому имени из перечисленных через запятую;
    pub fname_any: Option<String>,
    // выбрать всех, у кого указано имя (если 0) или не указано (если 1);
    pub fname_null: Option<IsNull>,

    // соответствие конкретной фамилии;
    pub sname_eq: Option<String>,
    // выбрать всех, чьи фамилии начинаются с переданного префикса;
    pub sname_starts: Option<String>,
    // выбрать всех, у кого указана фамилия (если 0) или не указана (если 1);
    pub sname_null: Option<IsNull>,

    // выбрать всех, у кого в телефоне конкретный код (три цифры в скобках);
    pub phone_code: Option<String>,
    // аналогично остальным;
    pub phone_null: Option<IsNull>,

    // всех, кто живёт в конкретной стране;
    pub country_eq: Option<String>,
    pub country_null: Option<IsNull>,

    // всех, кто живёт в конкретном городе;
    pub city_eq: Option<String>,
    // в любом из перечисленных через запятую городов;
    pub city_any: Option<String>,
    // аналогично остальным;
    pub city_null: Option<IsNull>,

    // выбрать всех, кто родился до указанной даты;
    pub birth_lt: Option<u32>,
    // после указанной даты;
    pub birth_gt: Option<u32>,
    // кто родился в указанном году;
    pub birth_year: Option<i32>,

    // все у кого есть премиум на текущую дату;
    pub premium_now: Option<u8>,
    // аналогично остальным;
    pub premium_null: Option<IsNull>,

    // выбрать всех, кто лайкал всех перечисленных пользователей
    // (в значении - перечисленные через запятые id);
    pub likes_contains: Option<String>,

    // выбрать всех, у кого есть все перечисленные интересы;
    pub interests_contains: Option<String>,
    // выбрать всех, у кого есть любой из перечисленных интересов;
    pub interests_any: Option<String>,
}

impl FilterRequest {
    #[inline]
    pub fn has_sex(&self) -> bool {
        self.sex_eq.is_some()
    }

    #[inline]
    pub fn has_status(&self) -> bool {
        self.status_eq.is_some() || self.status_neq.is_some()
    }

    #[inline]
    pub fn has_fname(&self) -> bool {
        self.fname_any.is_some()
            || self.fname_eq.is_some()
            || (match self.fname_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }

    #[inline]
    pub fn has_sname(&self) -> bool {
        self.sname_starts.is_some()
            || self.sname_eq.is_some()
            || (match self.sname_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }

    #[inline]
    pub fn has_phone(&self) -> bool {
        self.phone_code.is_some()
            || (match self.phone_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }

    #[inline]
    pub fn has_country(&self) -> bool {
        self.country_eq.is_some()
            || (match self.country_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }

    #[inline]
    pub fn has_city(&self) -> bool {
        self.city_any.is_some()
            || self.city_eq.is_some()
            || (match self.city_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }

    #[inline]
    pub fn has_birth(&self) -> bool {
        self.birth_gt.is_some() || self.birth_lt.is_some() || self.birth_year.is_some()
    }

    #[inline]
    pub fn has_premium(&self) -> bool {
        self.premium_now.is_some()
            || (match self.premium_null {
                Some(ref null) => null == &IsNull::NO,
                None => false,
            })
    }
}

#[derive(Deserialize, PartialEq, Eq, Debug)]
pub enum IsNull {
    #[serde(rename = "0")]
    NO = 0,
    #[serde(rename = "1")]
    YES = 1,
}

#[derive(Serialize)]
pub struct FilterResults {
    pub accounts: Vec<FilterResult>,
}

// TODO: city: Option<Option<String>>
#[derive(Serialize)]
pub struct FilterResult {
    pub id: u32,
    pub email: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fname: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sname: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub phone: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub birth: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub premium: Option<PremiumSpan>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub sex: Option<Sex>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub country: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub city: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<Status>,
}

fn validate_group_keys<T: AsRef<str>>(keys: &[T]) -> Result<(), ValidationError> {
    if keys.is_empty() {
        return Err(ValidationError::new("at least one key must be present"));
    }

    for key in keys {
        let k = key.as_ref();
        if k != "sex" && k != "status" && k != "interests" && k != "country" && k != "city" {
            return Err(ValidationError::new(
                "not known key in group keys (sex, status, interests, country and city are accepted)",
            ));
        }
    }

    Ok(())
}

#[derive(Deserialize, Validate)]
#[serde(deny_unknown_fields)]
pub struct RecommendRequest {
    // Служебное поле
    pub query_id: u32,

    // Фильтры по конкретным значениям
    #[validate(length(min = "1"))]
    pub country: Option<String>,
    #[validate(length(min = "1"))]
    pub city: Option<String>,

    // Дополнительные параметры запроса
    #[validate(range(min = "1", max = "20"))]
    pub limit: u8,
}

#[derive(Serialize)]
pub struct RecommendResults {
    pub accounts: Vec<RecommendResult>,
}

#[derive(Serialize)]
pub struct RecommendResult {
    pub id: u32,
    pub email: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fname: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sname: Option<String>,
    pub birth: u32,
    pub status: Status,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub premium: Option<PremiumSpan>,
}

use serde::de::{self, Deserializer, Visitor};
use std::fmt::{self, Display};
use std::iter::FromIterator;
use std::marker::PhantomData as Phantom;
use std::str::FromStr;

pub fn comma_separated<'de, V, T, D>(deserializer: D) -> Result<V, D::Error>
where
    V: FromIterator<T>,
    T: FromStr,
    T::Err: Display,
    D: Deserializer<'de>,
{
    struct CommaSeparated<V, T>(Phantom<V>, Phantom<T>);

    impl<'de, V, T> Visitor<'de> for CommaSeparated<V, T>
    where
        V: FromIterator<T>,
        T: FromStr,
        T::Err: Display,
    {
        type Value = V;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("string containing comma-separated elements")
        }

        fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            let iter = s.split(',').map(FromStr::from_str);
            Result::from_iter(iter).map_err(de::Error::custom)
        }
    }

    let visitor = CommaSeparated(Phantom, Phantom);
    deserializer.deserialize_str(visitor)
}
