#[macro_use]
extern crate criterion;
extern crate hlcup;

use criterion::Criterion;
use hlcup::{json::*, Db};

fn fill_db(db: &mut Db) {
    for id in 0..1_300_000 {
        db.insert(&Account {
            id: id,
            sex: Sex::M,
            email: format!("{}@gmail.com", id).to_string(),
            status: Status::FREE,
            birth: 0,
            joined: 0,
            city: None,
            country: None,
            premium: None,
            sname: None,
            fname: None,
            interests: vec![],
            likes: vec![],
            phone: None,
        })
        .unwrap();
    }

    for id in 1_300_001..1_320_000 {
        db.insert(&Account {
            id: id,
            sex: Sex::F,
            email: format!("{}@gmail.com", id).to_string(),
            status: Status::FREE,
            birth: 0,
            joined: 0,
            city: None,
            country: None,
            premium: None,
            sname: None,
            fname: None,
            interests: vec![],
            likes: vec![],
            phone: None,
        })
        .unwrap();
    }
}

fn filter(c: &mut Criterion) {
    {
        let mut db = Db::new();
        fill_db(&mut db);

        c.bench_function("exact", move |b| {
            b.iter(|| {
                db.filter(
                    &FilterRequest {
                        query_id: 0,
                        limit: 40,
                        sex_eq: Some(Sex::M),
                        email_domain: None,
                        email_lt: None,
                        email_gt: None,
                        status_eq: None,
                        status_neq: None,
                        fname_eq: None,
                        fname_any: None,
                        fname_null: None,
                        sname_eq: None,
                        sname_starts: None,
                        sname_null: None,
                        phone_code: None,
                        phone_null: None,
                        country_eq: None,
                        country_null: None,
                        city_eq: None,
                        city_any: None,
                        city_null: None,
                        birth_lt: None,
                        birth_gt: None,
                        birth_year: None,
                        premium_now: None,
                        premium_null: None,
                        likes_contains: None,
                        interests_contains: None,
                        interests_any: None,
                    },
                    0,
                )
            })
        });
    }
    {
        let mut db = Db::new();
        fill_db(&mut db);

        c.bench_function("full", move |b| {
            b.iter(|| {
                db.filter(
                    &FilterRequest {
                        query_id: 0,
                        limit: 40,
                        sex_eq: Some(Sex::F),
                        email_domain: None,
                        email_lt: None,
                        email_gt: None,
                        status_eq: None,
                        status_neq: None,
                        fname_eq: None,
                        fname_any: None,
                        fname_null: None,
                        sname_eq: None,
                        sname_starts: None,
                        sname_null: None,
                        phone_code: None,
                        phone_null: None,
                        country_eq: None,
                        country_null: None,
                        city_eq: None,
                        city_any: None,
                        city_null: None,
                        birth_lt: None,
                        birth_gt: None,
                        birth_year: None,
                        premium_now: None,
                        premium_null: None,
                        likes_contains: None,
                        interests_contains: None,
                        interests_any: None,
                    },
                    0,
                )
            })
        });
    }
}

criterion_group!(benches, filter);
criterion_main!(benches);
