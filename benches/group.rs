#[macro_use]
extern crate criterion;
extern crate hlcup;

use criterion::Criterion;
use hlcup::{json::*, Db};

fn fill_db(db: &mut Db) {
    for id in 0..1_320_000 {
        db.insert(&Account {
            id: id,
            sex: Sex::M,
            email: format!("{}@gmail.com", id).to_string(),
            status: Status::FREE,
            birth: 0,
            joined: 0,
            city: None,
            country: None,
            premium: None,
            sname: None,
            fname: None,
            interests: vec![],
            likes: vec![],
            phone: None,
        })
        .unwrap();
    }
}

fn group(c: &mut Criterion) {
    {
        let mut db = Db::new();
        fill_db(&mut db);
        c.bench_function("group sex", move |b| {
            b.iter(|| {
                db.group(&GroupRequest {
                    query_id: 0,
                    keys: vec!["sex".to_owned()],
                    country: None,
                    city: None,
                    interests: None,
                    sex: None,
                    status: None,
                    likes: None,
                    birth: None,
                    joined: None,
                    limit: 50,
                    order: Order::DESC,
                })
            })
        });
    }
}

criterion_group!(benches, group);
criterion_main!(benches);
